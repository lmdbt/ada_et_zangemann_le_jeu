# Projet Interdisciplinaire : Création d'un Jeu de Plateau basé sur l'Histoire "Ada & Zangemann"

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_af747e08f486f953993e4812e5e2b45c.jpg)

**Avant-propos**

<p style="text-align:right; color:gray; font-size:0.9em;">le 30/06/2024</p>

Ce depôt est davantage une présentation de projet qu'une version "Ready to print" d'un jeu "Ada & Zangemann".
Toutefois, sur la base de ce travail très riche réalisé par deux classes de 5e, nous allons essayer de produire une version finalisée et "ready to print" du jeu.


## Sommaire

- [Objectif du Projet](#objectif)
- [Présentation du projet](#presentation)
- [Version actuelle](#version)
- [Contexte et Motivation](#contexte)
- [Déroulement du Projet](#deroulement)
- [Collaboration avec Digipage](#collaboration)
- [Conclusion](#conclusion)
- [Licence](#licence)
- [Portfolio](#portfolio)
- [Remerciements et contributeurs](#remerciements)

<p id="objectif"></p>
## Objectif du projet

L'objectif de ce projet était de créer un jeu de plateau basé sur l'histoire [Ada & Zangemann](https://ada-lelivre.fr/) de Matthias Kirschner et Sandra Brandstätter avec les élèves de 5e. Ce projet interdisciplinaire a impliqué :
- la lecture de l'histoire,
- la création de cartes de jeu,
- la réflexion sur le gameplay,
- la construction du plateau de jeu.

Ce projet visait à développer les compétences de lecture, d'analyse, de créativité et de collaboration des élèves, tout en les initiant aux outils numériques pour la conception de jeux.

![Couverture du livre "Ada & Zangemann"](https://ada-lelivre.fr/images/couverture_ada_L800.jpg)

<p id="presentation"></p>
## Présentation du projet

- Au format [CodiMD](https://codimd.apps.education.fr/p/wVuq4Hxsb)
- Au format [FlipBook](https://eyssette.forge.apps.education.fr/flipbook/#https://codimd.apps.education.fr/s/E85EU5O2f)

<p id="version"></p>
## Version actuelle (juin 2024)

### Note sur la version présente dans ce dépôt

La version présente dans le dépôt n'a pas pu être testée suffisamment par les élèves, mais suffisamment pour qu'ils se rendent compte que des améliorations étaient nécessaires sans avoir le temps de les mettre en œuvre.

### Les règles

Les règles du jeu sont disponibles ici : [Règles du jeu](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/regles.html)

### Les cartes

Les cartes sont disponibles en version modifiable via l'application Creacarte ici : [CréaCarte - Version Ada & Zangemann](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/creacarte.html)

NB : Les illustrations des cartes "Casque" et "Pluie" ne sont pas présente dans le livre.

### Le plateau

Le plateau de base réalisé sur Géogebra est disponible ici : [Plateau - Cases - Géogébra](https://www.geogebra.org/m/cqhya7zc)

Le plateau finalisé au format LibreOffice Impression disponible ici : [Plateau - Final - ODP](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/plateau.odp) ou au format PDF disponible ici : [Plateau - Final - PDF](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/plateau.pdf)

<p id="contexte"></p>
## Contexte et Motivation

### L'histoire

L'histoire "Ada & Zangemann" aborde des notions importantes telles que le recyclage, l'indépendance numérique, le partage et la collaboration.

![Illustration de "Ada & Zangemann"](https://minio.apps.education.fr/codimd-prod/uploads/upload_aab189aae44d294b15e0a27c45b94fb2.jpg)

### Collaboration intraclasse et interclasse

Si les élèves d'une même classe ont collaboré pour aboutir à ce résultat. Les deux classes de 5e ont aussi travaillé ensemble. En effet, chaque classe ont eu une journée banalisée pour concevoir le jeu mais pas au même moment.
Fait marquant, la deuxième classe a bien voulu jouer le jeu et reprendre le travail réalisé par la première classe. Ainsi, le travail de collaboration et d'amélioration d'un contenu préexistant s'est déjà produit au sein même de notre établissement.

<p id="deroulement"></p>
## Déroulement du Projet

### Étape 1 : Lecture et Compréhension

**Activité** : Les élèves ont lu [l'histoire "Ada & Zangemann"](https://ada-lelivre.fr/).  
**Objectif** : Comprendre les thèmes et les personnages de l'histoire.

### Étape 2 : Proposition du Projet

**Activité** : Présenter le projet de création d'un jeu de plateau basé sur l'histoire.  
**Objectif** : Expliquer les différentes phases du projet et les attentes.

### Étape 3 : Réflexion sur le Gameplay

**Activité** : Discuter du gameplay, des règles du jeu et des mécanismes pour les améliorer.  
**Objectif** : Encourager la pensée critique et la collaboration.  

### Étape 4 : Création des Cartes de Jeu

**Activité** : Les élèves ont créé les cartes de jeu (avantage/obstacle, personnages...).  
**Objectif** : Stimuler la créativité et l'analyse des éléments clés de l'histoire.  
**Logiciel** : [CréaCarte - Version Ada & Zangemann](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/creacartes.html)

![Exemple de cartes de jeu](https://minio.apps.education.fr/codimd-prod/uploads/upload_478357aae3a8174c67e75c69835c6853.png)

### Étape 5 : Construction du Plateau

**Activité** : Les élèves ont dessiné le plateau de jeu au format A3 (sur papier puis au format vectoriel).  
**Objectif** : Mettre en pratique les compétences artistiques et de design.  
**Logiciel** : GeoGebra - [Plateau des élèves](https://www.geogebra.org/m/cqhya7zc)

![Exemple de plateau de jeu](https://minio.apps.education.fr/codimd-prod/uploads/upload_5e94ff362e43f92c8beefc3ae6725737.jpg)

### Étape 6 : Impression, Conception Matériel et Tests

**Activité** : Finaliser le design du jeu, imprimer le plateau et les cartes, concevoir le matériel nécessaire et réaliser des sessions de test.  
**Objectif** : Intégrer les outils numériques dans le projet et tester le jeu avec les élèves pour s'assurer de son bon fonctionnement et de son intérêt pédagogique.

<p id="collaboration"></p>
## Collaboration avec CodiMD ([Digipage](https://digipage.app))

Les élèves ont travaillé en collaboration sur un document type CodiMD ([Digipage](https://digipage.app) de la Digitale). Ils ont copié-collé les images et récupéré les URL des images pour créer les cartes. Ils ont également rédigé les règles et les contenus (cartes et plateau) du jeu, les créateurs de cartes consultant cette page pour la conception.

<p id="conclusion"></p>
## Conclusion

Ce projet a offert une opportunité unique d'intégrer plusieurs disciplines (français, mathématiques, arts plastiques, et technologie) dans une activité collaborative et créative. Il a permis de développer des compétences variées chez les élèves tout en les sensibilisant à l'importance de la liberté informatique, du recyclage et du partage, conformément aux thèmes abordés dans l'histoire "Ada & Zangemann".

<p id="licence"></p>
## Licence

Le projet est distribué sous la licence [Creative Commons Attribution-ShareAlike - CC-BY-SA - <b class="fa-brands fa-creative-commons"></b><b class="fa-brands fa-creative-commons-by"></b><b class="fa-brands fa-creative-commons-sa"></b>](https://creativecommons.org/licenses/by-sa/2.0/fr/deed.fr), ce qui signifie que vous êtes libre de partager, copier, distribuer et adapter le matériel tant que vous créditez les auteurs originaux et partagez vos contributions sous la même licence.

<p id="portfolio"></p>
## Portfolio

<div style="display: flex; flex-wrap: wrap;">
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_f5510a3d1431f03c63bce691258cc272.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_28ae089669a6dd63e63b8c68274843f4.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_dd87402c2c48e9e5b7ff1dfe79ac87fc.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_a639879cea8d12fc6a0761595ff1aa45.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_846aff3518d1893cb577bb5590eeb497.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_983f8ef56e343eb2a11643f0b4d7a405.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_d5701e324796ba0349a6700eec81e79b.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_0f812c7bc86e034b84fa90770e9c92f6.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_9f52f3f6e70613924a53590257fa1623.jpg" style="width: 100%;">
    </div>
    <div style="width: 45%; padding: 5px;">
        <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_450e3e6cbfac612bb7f1d84d76f01f6e.jpg" style="width: 100%;">
    </div>
</div>


<p id="remerciements"></p>
## Remerciements et contributeurs

Merci aux élèves de 5e du collège Victor Vasarely (année 2023-2024) pour leur engagement et aux collègues de lettres modernes Mme Cavarec et Mme Gérard pour leur investissement dans cette aventure !


