# Ada & Zangemann - Le Jeu

<div style="display:block; margin:auto; width:50%;">
<img src="https://digipage.app/uploads/324de3fc-4f10-493e-b75b-8eb69270b887.png">
</div>

## Introduction

**Zangemann le plus grand informaticien du monde entier a inventé les logiciels les plus performants.**
**Comme il est très égoïste, il détient tous les logiciels et refuse que quiconque les modifie.**
**Vous et vos ami.e.s ne pouvez pas créer des programmes pour le moment, mais vous pourrez le faire à l’aide des objets récupérés pour rivaliser avec Zangemann.**

**À vous de jouer !**

## Installation du jeu

- Chaque joueur pioche une carte personnage
- Distribuer 5 cartes à chaque joueur
- Chaque joueur reçoit une carte de cinq pièces d'or 
- Séparer les cartes obstacles en 2 tas, un tas de cartes obstacles uniquement et un tas à mélanger avec la pioche qui réunie toutes les autres cartes sauf les cartes objets et personnages.

## Règles du jeu

Zangemann est seul contre les autres joueurs qui forment une équipe. Chaque camp doit fabriquer les 3 objets qui permettront de créer l'ordinateur en or. Le camp qui atteint l'objectif gagne la partie.

Pour fabriquer les objets, il faut retrouver les 5 matériaux indiqués sur les cartes mode d'emploi.

- **Cartes Obstacles** : les cartes obstacles sont là pour vous empêcher de finir le jeu.
- **Cartes Avantages** : elles vous protègent des obstacles.
- **Cartes Skateboard** : piochées par Tony, ces cartes l'avantagent mais, désavantagent les autres joueurs qui la piochent.
- **matériau** : vous gagnez un materiau afin de construire un des 3 objets
- **cartes personnages** : votre rôle et vos pouvoirs dans le jeu

## Le plateau

- **Cases chaleur** : passez votre tour sauf si vous avez en votre possession une carte glace.
- **Cases maladroit** : piochez une carte obstacle.
- **Case bibliothèque** : piochez 2 cartes chance, si vous tombez sur une carte obstacle, repiochez.
- **Cases chance** : piochez une carte.
- **Cases musique inaudible** : passez votre tour sauf si vous utilisez une carte antibruit.


## Effets des cartes

- **casques antibruit** : *protège de la case « musique inaudible »*
- **protège-tibia** : *protège de la case « maladroit »*
- **chaos joyeux** : *tout le monde est heureux grâce aux inventions d’Ada : piochez une carte avantage*
- **« j’aide les adultes »** : *vous aidez les adultes avec leurs appareils : vous gagnez 50 euros*
- **radar** : *vous perdez de l’argent car vous avez fait un excès de vitesse en skate : 30 euros*
- **il pleut** : *la pluie ralentit le groupe : tous les joueurs reculent d’une case*
- **toboggan glissant** : *vous glissez sur le grand toboggan de Zangemann : vous reculez de trois cases en perdant un objet appartenant au groupe (choisissez lequel)*
- **loi de protection des logiciels** : *vous trafiquez des logiciels : vous êtes coincé.e sur votre case pendant deux tours*
- **spam Zangemann** : *Zangemann vous spamme d’appels : vous passez votre tour le temps de créer un logiciel*
- **skate** : *si vous êtes le skateur vous allez vite et au passage récupérez un petit matériau, si vous n’êtes pas le skateur vous tombez et vous perdez un petit matériau par terre*
- **glace** : *vous dépensez 5 euros pour acheter la glace. Si vous tombez sur la case « chaleur », la glace vous sauve*

## Déroulement du jeu

Placez vous sur le point de départ. Pour vous déplacer, lancer le dé et avancer le nombre de cases correspondant. Vous pouvez vous déplacer librement sur le plateau qui représente la ville, mais toujours en suivant les routes.

Quand votre tour arrive, utilisez ou débarassez vous d'une carte pour en piocher une nouvelle. Vous devez toujours avoir 5 cartes en main, sauf Marie qui possède 6 cartes grace à son pouvoir.
Si vous tombez sur une carte mode d'emploi, gardez la de coté et tentez de réunir les materiaux de la liste. Vous pouvez toujours vous débarasser de la carte mode d'emploi durant la partie.

## Les pouvoirs

- **Mère d'Ada** : elle peut échanger 1 de ses cartes avec une de Zangemann
- **Le Président** : il est immunisé contre les pouvoirs de Zangemann
- **Marie** : elle peut avoir 6 cartes en main
- **Tony** : il peut avancer de 1 ou 2 cases en plus 5 fois dans la partie
- **Alan** : il peut regarder l'inventaire de Zangemann 1 fois dans la partie
- **Konrad** : il peut suprimer 1 matériau de la liste de matériaux pour fabriquer un objet
- **Ada** : elle a besoin de 2 matériaux en moins pour fabriquer un objet
- **La conductrice de bus** : elle se déplace où elle veut sur le plateau avec un personnage de son choix 5 fois dans la partie. 
- **Zangemann** : il peut bloquer 1 personne 5 fois dans la partie et peut casser un objet construit par le camp adverse. Les cartes matériau et la carte mode d'emploi sont alors redistribuées dans la pioche
- **Mme Gernet** : peut aller à la bibliothèque 5 fois dans la partie quand elle veut


