# Projet de Jeu de Plateau "Ada & Zangemann"

Pour plus d'information, consulter la [page principal du projet](https://lmdbt.forge.apps.education.fr/ada_et_zangemann_le_jeu/).

**Date :** 30/06/2024

## Description

Ce dépôt présente le projet de création d'un jeu de plateau basé sur l'histoire "Ada & Zangemann", réalisé par des élèves de 5e. Le projet inclut la lecture de l'histoire, la conception des cartes de jeu, la réflexion sur le gameplay, et la construction du plateau.

## Contenu du dépôt

- **Étapes de conception** : Description des différentes phases du projet.
- **Cartes de jeu** : Fichiers des cartes créées par les élèves.
- **Plateau de jeu** : Design du plateau en format vectoriel.
- **Règles du jeu** : Documentation des règles élaborées par les élèves.

## Objectif

Bien que ce dépôt ne soit pas encore une version "Ready to print", il constitue une base solide pour finaliser le jeu. Nous allons travailler à partir de ce riche contenu pour produire une version finalisée et imprimable.

## Licence

Le projet est distribué sous la licence Creative Commons Attribution-ShareAlike (CC-BY-SA), ce qui signifie que vous êtes libre de partager, copier, distribuer et adapter le matériel tant que vous créditez les auteurs originaux et partagez vos contributions sous la même licence.

## Remerciements

Merci aux élèves de 5e du collège Victor Vasarely de Collinée pour leur investissement et à tous les collègues pour leur soutien.
